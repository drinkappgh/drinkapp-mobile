import React from 'react';
import { RootStack } from './config/router';
import navigation from './services/navigation';

export default class App extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
    username: undefined,
  };

  render() {
    return (
      <RootStack ref={navigatorRef => {
              navigation.setTopLevelNavigator(navigatorRef);
              }} 
            />
        );
  }

  
}