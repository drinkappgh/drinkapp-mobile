import * as axios from 'axios';
import { login } from '../services/auth';
import { retrieveItem, setItem } from '../services/localstorage';

async function getCategories() {
  let categories;
  await axios
    .get('http://api.drinkappgh.com/api/Category?pageIndex=1&pageSize=10')
    .then(function(response) {
      categories = response.data;
    })
    .catch(function(error) {
      console.error('Api#getCategories error: ' + error.message);
    });
  return categories;
}

async function getProductBaskets(category) {
  let productbaskets;
  await axios
    .get(
      'http://api.drinkappgh.com/api/ProductBasket?pageIndex=1&pageSize=10&category=' +
        category
    )
    .then(function(response) {
      productbaskets = response.data;
    })
    .catch(function(error) {
      console.error('Api#getProducts error: ' + error.message);
    });
  return productbaskets;
}

async function getProduct(productId) {
  let product;
  await axios
    .get(
      'http://api.drinkappgh.com/api/productBasket/' +
        productId
    )
    .then(function(response) {
      product = response.data;
    })
    .catch(function(error) {
      console.error('Api#getProduct error: ' + error.message);
    });
  return product;
}

async function addToCart(productId, quantity) {
  let addCartRequest;
    var authOptions = {
    method: 'POST',
    url: 'http://api.drinkappgh.com/api/Cart/AddItemToCart?productId=' + productId + '&quantity=' +
        quantity,
    headers: {
        'Authorization': 'Basic Y2xpZW50OnNlY3JldA=='
    }
  };
  await axios
    .post(authOptions)
    .then(function(response) {
      addCartRequest = response.data;
    })
    .catch(function(error) {
      console.error('Api#addToCart error: ' + error.message);
      //login();
    });
  return addCartRequest;
}

export { getCategories, getProductBaskets, getProduct, addToCart };
