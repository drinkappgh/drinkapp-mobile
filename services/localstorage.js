import { AsyncStorage} from 'react-native';

async function retrieveItem(key) {
    try {
      const retrievedItem =  await AsyncStorage.getItem(key);
      const item = JSON.parse(retrievedItem);
      return item;
    } catch (error) {
       console.error('AsyncStorage#retrieveItem error: ' + error.message);
    }
}

async function setItem(key, value) {
    try {
        await AsyncStorage.setItem(key, JSON.stringify(value));
        return 'success';
    } catch (error) {
        console.error('AsyncStorage#setItem error: ' + error.message);
    }
}

async function removeItem(key, value) {
    try {
        await AsyncStorage.removeItem(key);
        return 'success';
    } catch (error) {
        console.error('AsyncStorage#removeItem error: ' + error.message);
    }
}

export{retrieveItem, setItem, removeItem};