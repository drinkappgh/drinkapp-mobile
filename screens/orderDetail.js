import React from 'react';

import {
  NavigationBar,
  Button,
  TextInput,
  Row,
  View,
  Image,
  ImageBackground,
  Tile,
  Title,
  Subtitle,
  Divider,
  Caption,
  Screen,
  ListView,
  TouchableOpacity,
  Card,
  GridRow,
} from '@shoutem/ui';
import { Text, StyleSheet, TouchableHighlight, ScrollView } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import Modal from 'react-native-modal';
import UserAvatar from 'react-native-user-avatar';
import navigation from '../services/navigation';

export class orderDetailScreen extends React.Component {
  static navigationOptions = {
    drawerLabel: 'OrderDetail',
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const restaurants = this.state.restaurants;
    // Group the restaurants into rows with 2 columns, except for the
    // first restaurant. The first restaurant is treated as a featured restaurant
    let isFirstArticle = true;
    const groupedData = GridRow.groupByRows(restaurants, 1, () => {
      return 1;
    });

    return (
      <Screen style={styles.container}>
        <ImageBackground
style={{ width: 375, height: 70, backgroundColor: '#2c616e' }}>
          <NavigationBar
            hasHistory
            styleName="clear"
            leftComponent={
              <Ionicons
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                }}
                name="md-menu"
                size={25}
                color="white"
                style={{ marginLeft: 20 }}
              />
            }
            centerComponent={<Title>Your Order</Title>}
            rightComponent={
              <Ionicons
                onPress={() => {
                  navigation.navigate('Cart');
                }}
                name="md-cart"
                size={25}
                color="white"
                style={{ marginRight: 20 }}
              />
            }
          />
        </ImageBackground>
        <ScrollView>
          <View style={{ marginTop: 22, alignItems: 'center' }}>
            <Card
              style={{
                height: 350,
                width: 300,
                alignItems: 'center',
                marginTop: 20,
                paddingTop: 50,
              }}>
              <Ionicons
                onPress={() => {}}
                name="ios-clock-outline"
                size={100}
                color="red"
                style={{ marginTop: 20 }}
              />
              <View styleName="content" style={{ alignItems: 'center' }}>
                <Subtitle>Your Drinks Are On Their Way</Subtitle>
                <Caption>Estimated Arrival Time</Caption>
                <Caption>16:00 Today</Caption>
                <Caption>Manet Ville, Hse 3A, Spintex road</Caption>
              </View>
              {/*<Button styleName="full-width secondary" >
                <Text style={{color: 'white'}}>Add Note</Text>
              </Button>
              <Button styleName="full-width secondary" style={{marginTop: 5, height: 30}}>
                <Text  style={{color: 'white'}}>Send as gift to friend</Text>
              </Button>*/}
            </Card>
            <Card
              style={{
                height: 80,
                width: 300,
                alignItems: 'center',
                marginTop: 60
              }}>
                <Row>
                  <Image
                    styleName="small rounded-corners"
                    source={{ uri: 'https://s3-us-west-2.amazonaws.com/dave-vortex/grey/drink_1.jpg' }}
                  />
                  <View styleName="vertical stretch space-between">
                    <Subtitle>Four Cousins</Subtitle>
                    <Caption>Sweet Wine  ·  GHS 20</Caption>
                  </View>
                </Row>
            </Card>
          </View>
        </ScrollView>
      </Screen>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
