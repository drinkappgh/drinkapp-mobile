import React from 'react';
import Auth0 from 'react-native-auth0';

import {
  NavigationBar,
  TextInput,
  Button,
  Overlay,
  Heading,
  Icon,
  Row,
  View,
  Image,
  ImageBackground,
  Tile,
  Title,
  Subtitle,
  Divider,
  Caption,
  Screen,
  ListView,
  TouchableOpacity,
  Card,
  GridRow,
} from '@shoutem/ui';
import { Text, StyleSheet, TouchableHighlight } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Modal from 'react-native-modal';
import navigation from '../services/navigation';
import { getProductBaskets } from '../services/api';

export class ProductsScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
    this.state = {
      productBaskets: [],
    };
  }

  loadProductBaskets(){
    getProductBaskets(this.props.navigation.getParam('category', '')).then( productBaskets => {
      //alert(JSON.stringify(productBaskets));
      this.setState({ productBaskets: productBaskets.items });
    });
  }

  renderRow(productBasket) {
    return (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Product', { productId:  productBasket.products[0].id});
        }}>
        <Row>
          <Image
            styleName="small rounded-corners"
            source={{
              uri:
                productBasket.images[0] ? productBasket.images[0].url : '',
            }}
          />
          <View styleName="vertical stretch space-between">
            <Subtitle>{productBasket.name}</Subtitle>
            <Caption>{productBasket.subCategory.name}</Caption>
            <View styleName="horizontal">
              <Subtitle styleName="md-gutter-right">GHS {productBasket.products[0].price}</Subtitle>
            </View>
          </View>
          <Ionicons
            onPress={() => {
              this.props.navigation.goBack();
            }}
            name="md-basket"
            size={25}
            color="black"
            style={{ marginRight: 20 }}
          />
        </Row>
      </TouchableOpacity>
    );
  }

  render() {
    const productBaskets = this.state.productBaskets;

    return (
      <Screen>
        <ImageBackground
style={{ width: 375, height: 70, backgroundColor: '#2c616e' }}>
          <NavigationBar
            hasHistory
            styleName="clear"
            leftComponent={
              <Ionicons
                onPress={() => {
                  this.props.navigation.goBack();
                }}
                name="md-arrow-back"
                size={25}
                color="white"
                style={{ marginLeft: 20 }}
              />
            }
            centerComponent={<Title>DrinkApp</Title>}
            rightComponent={
              <Ionicons
                onPress={() => {
                  navigation.navigate('Cart');
                }}
                name="md-cart"
                size={25}
                color="white"
                style={{ marginRight: 20 }}
              />
            }
          />
        </ImageBackground>
        {/*<TextInput placeholder={'What are you having'} onChangeText={() => {}} />*/}
        <ListView data={productBaskets} renderRow={this.renderRow} />
      </Screen>
    );
  }

  componentDidMount() {
      this.loadProductBaskets();
  }

}

const styles = StyleSheet.create({});
