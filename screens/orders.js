import React from 'react';
import Auth0 from 'react-native-auth0';

import {
  NavigationBar,
  TextInput,
  Button,
  Overlay,
  Heading,
  Icon,
  Row,
  View,
  Image,
  ImageBackground,
  Tile,
  Title,
  Subtitle,
  Divider,
  Caption,
  Screen,
  ListView,
  TouchableOpacity,
  Card,
  GridRow,
} from '@shoutem/ui';
import { Text, StyleSheet, TouchableHighlight } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Modal from 'react-native-modal';
import navigation from '../services/navigation';

export class OrdersScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
    this.state = {
      restaurants: [
        {
          name: 'Gaspar Brasserie',
          address: '185 Sutter St, San Francisco, CA 94109',
          image: {
            url:
              'https://shoutem.github.io/static/getting-started/restaurant-1.jpg',
          },
        }
      ],
    };
  }

  renderRow(restaurant) {
    return (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('OrderDetail');
        }}>
        <Row>
          <Image
            styleName="small rounded-corners"
            source={{
              uri:
                'https://s3-us-west-2.amazonaws.com/dave-vortex/grey/drink_1.jpg',
            }}
          />
          <View styleName="vertical stretch space-between">
            <Subtitle>Order on 24th August, 2018 at 2:30</Subtitle>
            <View styleName="horizontal">
              <Subtitle styleName="md-gutter-right">GHS120.00</Subtitle>
            </View>
          </View>
          <Ionicons
            name="md-basket"
            size={25}
            color="black"
            style={{ marginRight: 20 }}
          />
        </Row>
      </TouchableOpacity>
    );
  }

  render() {
    const restaurants = this.state.restaurants;

    return (
      <Screen>
        <ImageBackground
style={{ width: 375, height: 70, backgroundColor: '#2c616e' }}>
          <NavigationBar
            hasHistory
            styleName="clear"
            leftComponent={
              <Ionicons
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                }}
                name="md-menu"
                size={25}
                color="white"
                style={{ marginLeft: 20 }}
              />
            }
            centerComponent={<Title>DrinkApp</Title>}
            rightComponent={
              <Ionicons
                onPress={() => {
                  navigation.navigate('Cart');
                }}
                name="md-cart"
                size={25}
                color="white"
                style={{ marginRight: 20 }}
              />
            }
          />
        </ImageBackground>
        <ListView data={restaurants} renderRow={this.renderRow} />
      </Screen>
    );
  }
}

const styles = StyleSheet.create({});
