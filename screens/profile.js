import React from 'react';

import {
  NavigationBar,
  Button,
  TextInput,
  Row,
  View,
  Image,
  ImageBackground,
  Tile,
  Title,
  Subtitle,
  Divider,
  Caption,
  Screen,
  ListView,
  TouchableOpacity,
  Card,
  GridRow,
} from '@shoutem/ui';
import { Text, StyleSheet, TouchableHighlight } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import Modal from 'react-native-modal';
import UserAvatar from 'react-native-user-avatar';
import navigation from '../services/navigation';

export class ProfileScreen extends React.Component {
  static navigationOptions = {
    drawerLabel: 'Profile',
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const restaurants = this.state.restaurants;
    // Group the restaurants into rows with 2 columns, except for the
    // first restaurant. The first restaurant is treated as a featured restaurant
    let isFirstArticle = true;
    const groupedData = GridRow.groupByRows(restaurants, 1, () => {
      return 1;
    });

    return (
      <Screen style={styles.container}>
        <ImageBackground
style={{ width: 375, height: 70, backgroundColor: '#2c616e' }}>
          <NavigationBar
            hasHistory
            styleName="clear"
            leftComponent={
              <Ionicons
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                }}
                name="md-menu"
                size={25}
                color="white"
                style={{ marginLeft: 20 }}
              />
            }
            centerComponent={<Title>DrinkApp Profile</Title>}
            rightComponent={
              <Ionicons
                onPress={() => {
                  navigation.navigate('Cart');
                }}
                name="md-cart"
                size={25}
                color="white"
                style={{ marginRight: 20 }}
              />
            }
          />
        </ImageBackground>
        <View style={{ marginTop: 22, alignItems: 'center' }}>
          <Card
            style={{
              height: 350,
              width: 300,
              alignItems: 'center',
              marginTop: 20,
              paddingTop: 50,
            }}>
            <UserAvatar name={'David Komlah'} size={100} />
            <View styleName="content" style={{ alignItems: 'center' }}>
              <Subtitle>David Komlah</Subtitle>
              <Caption>Spintex Road</Caption>
              <Caption>Loves Red Wine</Caption>
              <Ionicons
                onPress={() => {}}
                name="md-add"
                size={25}
                color="red"
                style={{ marginTop: 20 }}
              />
            </View>
          </Card>
        </View>
      </Screen>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
