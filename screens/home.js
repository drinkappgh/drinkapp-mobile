import React from 'react';

import {
  NavigationBar,
  TextInput,
  Row,
  View,
  Image,
  ImageBackground,
  Tile,
  Title,
  Subtitle,
  Divider,
  Caption,
  Screen,
  ListView,
  TouchableOpacity,
  Card,
  Button,
  GridRow,
  Overlay,
} from '@shoutem/ui';
import { ScrollView, Text, StyleSheet, TouchableHighlight } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import Modal from 'react-native-modal';
import navigation from '../services/navigation';
import { retrieveItem, setItem } from '../services/localstorage';
import { getCategories } from '../services/api';
import { AuthSession } from 'expo';
import jwtDecoder from 'jwt-decode';

const auth0ClientId = '49PNs68BFwc4wGSq683qyRRYfGqvH7kA';
const auth0Domain = 'https://greyauth.auth0.com';

/**
 * Converts an object to a query string.
 */
function toQueryString(params) {
  return '?' + Object.entries(params)
    .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    .join('&');
}

export class HomeScreen extends React.Component {
  static navigationOptions = {
    drawerLabel: 'Shop',
    header: null,
  };
  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
    this.state = {
      categories: [],
      serveLegalNotice: false,
    };
    this.checkLegalNotice();
    //this.checkLoginStatus();
  }

    _loginWithAuth0 = async () => {
    const redirectUrl = AuthSession.getRedirectUrl();
    console.log(`Redirect URL (add this to Auth0): ${redirectUrl}`);
    const result = await AuthSession.startAsync({
      authUrl: `${auth0Domain}/authorize` + toQueryString({
        client_id: auth0ClientId,
        response_type: 'token',
        scope: 'openid name',
        redirect_uri: redirectUrl,
      }),
    });

    console.log(result);
    if (result.type === 'success') {
      this.handleParams(result.params);
    }
  }

  handleParams = (responseObj) => {
    if (responseObj.error) {
      alert('Error', responseObj.error_description
        || 'something went wrong while logging in');
      return;
    }
    const encodedToken = responseObj.id_token;
    const decodedToken = jwtDecoder(encodedToken);
    const username = decodedToken.name;
    setItem('userToken', encodedToken);
  }

  checkLegalNotice() {
    retrieveItem('legalNotice').then(legalNotice => {
      let serveLegalNotice;
      if (legalNotice == null) {
        serveLegalNotice = true;
      } else {
        serveLegalNotice = false;
      }
      this.setState({ serveLegalNotice: serveLegalNotice });
    });
  }

  

  legalNoticeAcknowledged() {
    setItem('legalNotice', true);
    this.checkLegalNotice();
  }

  checkLoginStatus() {
    retrieveItem('userToken').then(userToken => {
      if (userToken == null) {
        this._loginWithAuth0();
      } 
    });
  }

  loadCategories(){
    getCategories().then( categories => {
      this.setState({ categories: categories.items });
    });
  }

  renderRow(rowData, sectionId, index) {
    const cellViews = rowData.map((category, id) => {
      return (
        <TouchableOpacity
          key={id}
          styleName="flexible"
          onPress={() => {
            navigation.navigate('Products', { categoryId:  category.name});
          }}>
          <Card styleName="flexible">
            <Image
              styleName="medium-wide"
              source={{ uri: category.images[0].imageUri }}
            />
            <View styleName="content">
              <Subtitle numberOfLines={3}>{category.name}</Subtitle>
              <View styleName="horizontal">
                <Caption styleName="collapsible" numberOfLines={2}>
                  {category.description}
                </Caption>
              </View>
            </View>
          </Card>
        </TouchableOpacity>
      );
    });

    return <GridRow columns={2}>{cellViews}</GridRow>;
  }

  render() {
    const categories = this.state.categories;
    const groupedData = GridRow.groupByRows(categories, 2, () => {
      return 1;
    });

    return (
      <Screen>
        <Modal
          animationType="slide"
          visible={this.state.serveLegalNotice}
          transparent={true}
          backdropColor={'black'}
          backdropOpacity={1}>
          <View style={{ height: 170, backgroundColor: 'white' }}>
            <View>
              <Row>
                <View
                  styleName="vertical stretch space-between"
                  style={{ height: 120, backgroundColor: 'white' }}>
                  <Subtitle>
                    You have to be 18 and above to purchase alcoholic drinks.
                  </Subtitle>
                  <Caption>
                    Confirm you are of the legal age to purchase drinks.
                  </Caption>
                  <View styleName="horizontal">
                    <Button
                      styleName="confirmation secondary"
                      onPress={() => this.legalNoticeAcknowledged()}>
                      <Text style={{ color: 'white' }}>I confirm</Text>
                    </Button>
                  </View>
                </View>
              </Row>
            </View>
          </View>
        </Modal>
        <ImageBackground
          style={{ width: 375, height: 70, backgroundColor: '#2c616e' }}>
          <NavigationBar
            styleName="clear"
            leftComponent={
              <Ionicons
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                }}
                name="md-menu"
                size={25}
                color="white"
                style={{ marginLeft: 20 }}
              />
            }
            centerComponent={<Title>DrinkApp</Title>}
            rightComponent={
              <Ionicons
                onPress={() => {
                  navigation.navigate('Cart');
                }}
                name="md-cart"
                size={25}
                color="white"
                style={{ marginRight: 20 }}
              />
            }
          />
        </ImageBackground>
        <ImageBackground
          styleName="large-banner"
          source={{
            uri:
              'https://s3-us-west-2.amazonaws.com/dave-vortex/grey/grey-dash.jpg',
          }}>
          <Tile>
            <Overlay styleName="image-overlay">
              <Title styleName="sm-gutter-horizontal">Guess What</Title>
              <Subtitle>Drinking milk can cure hangovers</Subtitle>
            </Overlay>
          </Tile>
        </ImageBackground>
        <ListView data={groupedData} renderRow={this.renderRow} />
      </Screen>
    );
  }

  componentDidMount() {
      this.loadCategories();
  }
}
