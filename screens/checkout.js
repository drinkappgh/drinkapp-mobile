import React from 'react';

import {
  NavigationBar,
  Button,
  TextInput,
  Row,
  View,
  Image,
  ImageBackground,
  Tile,
  Title,
  Subtitle,
  Divider,
  Caption,
  Screen,
  ListView,
  TouchableOpacity,
  Card,
  GridRow,
} from '@shoutem/ui';
import { Text, StyleSheet, TouchableHighlight } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import Modal from 'react-native-modal';
import navigation from '../services/navigation';

export class CheckoutScreen extends React.Component {
  static navigationOptions = {
    drawerLabel: 'Cart',
  };

  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
    this.state = {
      restaurants: [
        {
          name: 'Welcome to Drink App',
          address: 'What are you having',
          image: {
            url:
              'https://s3-us-west-2.amazonaws.com/dave-vortex/grey/drink_1.jpg',
          },
        },
      ],
    };
  }

  renderRow(rowData, sectionId, index) {
    // rowData contains grouped data for one row,
    // so we need to remap it into cells and pass to GridRow
    const cellViews = rowData.map((restaurant, id) => {
      return (
        <TouchableOpacity
          key={id}
          styleName="flexible"
          onPress={() => {
            navigation.navigate('Product');
          }}>
          <Row>
            <Image
              styleName="small rounded-corners"
              source={{
                uri:
                  'https://s3-us-west-2.amazonaws.com/dave-vortex/grey/drink_1.jpg',
              }}
            />
            <View styleName="vertical stretch space-between">
              <Subtitle>Don Chappier</Subtitle>
              <Subtitle styleName="md-gutter-right" style={{ color: 'grey' }}>
                Wine
              </Subtitle>
              <View styleName="horizontal">
                <Subtitle styleName="md-gutter-right" style={{ color: 'red' }}>
                  GHc 100.00 x 2
                </Subtitle>
              </View>
            </View>
            <MaterialIcons name="remove" size={25} color="red" />
          </Row>
        </TouchableOpacity>
      );
    });

    return <GridRow columns={1}>{cellViews}</GridRow>;
  }

  render() {
    const restaurants = this.state.restaurants;
    // Group the restaurants into rows with 2 columns, except for the
    // first restaurant. The first restaurant is treated as a featured restaurant
    let isFirstArticle = true;
    const groupedData = GridRow.groupByRows(restaurants, 1, () => {
      return 1;
    });

    return (
      <Screen style={styles.container}>
        <ImageBackground
style={{ width: 375, height: 70, backgroundColor: '#2c616e' }}>
          <NavigationBar
            hasHistory
            styleName="clear"
            leftComponent={
              <Ionicons
                onPress={() => {
                  navigation.navigate('Home');
                }}
                name="md-arrow-back"
                size={25}
                color="white"
                style={{ marginLeft: 20 }}
              />
            }
            centerComponent={<Title>DrinkApp</Title>}
            rightComponent={
              <Ionicons
                onPress={() => {
                  navigation.navigate('Cart');
                }}
                name="md-cart"
                size={25}
                color="white"
                style={{ marginRight: 20 }}
              />
            }
          />
        </ImageBackground>
        <Text
          style={{
            backgroundColor: 'white',
            height: 40,
            paddingTop: 20,
            fontWeight: 'bold',
            fontSize: 15,
            textAlign: 'center',
          }}>
          Total: <Text style={{ color: 'red' }}> GHc 200 </Text>
        </Text>
        <ListView data={groupedData} renderRow={this.renderRow} />
        <Button style={{ height: 50, backgroundColor: '#000' }}>
          <Text style={{ color: '#fff' }}>Proceed To Checkout</Text>
        </Button>
      </Screen>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
