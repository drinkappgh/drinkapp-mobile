import React from 'react';

import {
  NavigationBar,
  Button,
  TextInput,
  Row,
  View,
  Image,
  ImageBackground,
  Tile,
  Title,
  Subtitle,
  Divider,
  Caption,
  Screen,
  ListView,
  TouchableOpacity,
  Card,
  GridRow,
  ScrollView,
} from '@shoutem/ui';
import { Text, StyleSheet, TouchableHighlight } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import Modal from 'react-native-modal';
import UserAvatar from 'react-native-user-avatar';
import navigation from '../services/navigation';

export class SupportScreen extends React.Component {
  static navigationOptions = {
    drawerLabel: 'Support',
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const restaurants = this.state.restaurants;
    // Group the restaurants into rows with 2 columns, except for the
    // first restaurant. The first restaurant is treated as a featured restaurant
    let isFirstArticle = true;
    const groupedData = GridRow.groupByRows(restaurants, 1, () => {
      return 1;
    });

    return (
      <Screen style={styles.container}>
        <ImageBackground
style={{ width: 375, height: 70, backgroundColor: '#2c616e' }}>
          <NavigationBar
            hasHistory
            styleName="clear"
            leftComponent={
              <Ionicons
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                }}
                name="md-menu"
                size={25}
                color="white"
                style={{ marginLeft: 20 }}
              />
            }
            centerComponent={<Title>DrinkApp Support</Title>}
            rightComponent={
              <Ionicons
                onPress={() => {
                  navigation.navigate('Cart');
                }}
                name="md-cart"
                size={25}
                color="white"
                style={{ marginRight: 20 }}
              />
            }
          />
        </ImageBackground>
        <ScrollView>
          <View style={{ marginTop: 22, alignItems: 'center' }}>
            <Card
              style={{
                height: 350,
                width: 300,
                alignItems: 'center',
                marginTop: 10,
                paddingTop: 50,
              }}>
              <View styleName="content" style={{ alignItems: 'center' }}>
                <Subtitle>FAQs</Subtitle>
                <Caption>Spintex Road</Caption>
                <Caption>Loves Red Wine</Caption>
              </View>
            </Card>
            <Card
              style={{
                height: 140,
                width: 300,
                alignItems: 'center',
                marginTop: 10,
                paddingTop: 20,
              }}>
              <View styleName="content" style={{ alignItems: 'center' }}>
                <Subtitle>Contact Center</Subtitle>
                <Caption>+233 207110652</Caption>
                <Caption>Tema community 9</Caption>
              </View>
            </Card>
            <Card
              style={{
                height: 350,
                width: 300,
                alignItems: 'center',
                marginTop: 10,
                paddingTop: 50,
              }}>
              <View styleName="content" style={{ alignItems: 'center' }}>
                <Subtitle>Terms and Conditions</Subtitle>
                <Caption>1. No refunds for opened drinks</Caption>
                <Caption>2. Must be 18 and above to purchase alcohol</Caption>
                <Caption>3. Can not send alcohol to teenagers as a gift</Caption>
              </View>
            </Card>
            <Card
              style={{
                height: 150,
                width: 300,
                alignItems: 'center',
                marginTop: 10,
                paddingTop: 50
              }}>
              <View styleName="content" style={{ alignItems: 'center' }}>
                <Subtitle>Privacy</Subtitle>
                <Caption>We don't sell your data to third parties</Caption>
              </View>
            </Card>
          </View>
        </ScrollView>
      </Screen>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
