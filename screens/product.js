import React from 'react';
import Auth0 from 'react-native-auth0';

import {
  NavigationBar,
  TextInput,
  Button,
  Overlay,
  Heading,
  Icon,
  Row,
  View,
  Image,
  ImageBackground,
  Tile,
  Title,
  Subtitle,
  Divider,
  Caption,
  Screen,
  ListView,
  TouchableOpacity,
  Card,
  GridRow,
} from '@shoutem/ui';
import { Text, StyleSheet, TouchableHighlight } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Modal from 'react-native-modal';
import navigation from '../services/navigation';
import { getProduct, addToCart } from '../services/api';

export class ProductScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      productBasket: null,
      quantity: 1
    };
  }

  loadProduct() {
    getProduct(this.props.navigation.getParam('productId', '')).then(
      product => {
        //alert(JSON.stringify(product));
        this.setState({ productBasket: product });
      }
    );
  }

  putInCart() {
    if (this.state.productBasket) {
      addToCart(this.state.productBasket.products[0].id, this.state.quantity).then(product => {
        //alert(JSON.stringify(product));
        this.setState({ productBasket: product });
      });
    }
  }

  render() {
    return (
      <Screen style={styles.container}>
        <ImageBackground
          style={{ width: 375, height: 70, backgroundColor: '#2c616e' }}>
          <NavigationBar
            hasHistory
            styleName="clear"
            leftComponent={
              <Ionicons
                onPress={() => {
                  this.props.navigation.goBack();
                }}
                name="md-arrow-back"
                size={25}
                color="white"
                style={{ marginLeft: 20 }}
              />
            }
            centerComponent={<Title>DrinkApp</Title>}
            rightComponent={
              <Ionicons
                onPress={() => {
                  navigation.navigate('Cart');
                }}
                name="md-cart"
                size={25}
                color="white"
                style={{ marginRight: 20 }}
              />
            }
          />
        </ImageBackground>
        <Tile>
          <Image
            styleName="large-banner"
            style={{ height: 300 }}
            source={{
              uri: this.state.productBasket
                ? this.state.productBasket.images[0]
                  ? this.state.productBasket.images[0].url
                  : ''
                : '',
            }}
          />
          <View styleName="content">
            <Title>
              {this.state.productBasket ? this.state.productBasket.name : ''}
            </Title>
            <View styleName="horizontal space-between">
              <Text style={{ color: 'red' }}>
                Ghc{' '}
                {this.state.productBasket
                  ? this.state.productBasket.products[0].price
                  : ''}
              </Text>
              <Caption>
                {this.state.productBasket
                  ? this.state.productBasket.description
                  : ''}
              </Caption>
            </View>
            <TextInput
              placeholder={'1'}
              onChangeText={(text) => this.setState({quantity: text})}
              defaultValue="1"
              keyboardType="numeric"
            />
            <View styleName="horizontal">
              <Button
                styleName="confirmation"
                title="hey there"
                style={{ height: 40, borderColor: 'red' }}
                onPress={() => {
                  //this.putInCart();
                }}>
                <Text>Add To Cart</Text>
              </Button>

              <Button
                styleName="confirmation secondary"
                style={{ height: 40 }}
                onPress={() => {
                  this.props.navigation.goBack();
                }}>
                <Text style={{ color: 'white' }}>Cancel</Text>
              </Button>
            </View>
          </View>
        </Tile>
      </Screen>
    );
  }

  componentDidMount() {
    this.loadProduct();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white',
  },
});
