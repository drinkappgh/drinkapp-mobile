import { createStackNavigator, createDrawerNavigator } from 'react-navigation';
import { HomeScreen } from '../screens/home';
import { ProductsScreen } from '../screens/products';
import { ProductScreen } from '../screens/product';
import { CartScreen } from '../screens/cart';
import { OrdersScreen } from '../screens/orders';
import { orderDetailScreen } from '../screens/orderDetail';
import { SupportScreen } from '../screens/support';
import { ProfileScreen } from '../screens/profile';
import SideNav from './sideNav';

export const RootStack = createDrawerNavigator({
  Home: {
    screen: createStackNavigator(
      {
        Home: {
          screen: HomeScreen,
        },
        Products: {
          screen: ProductsScreen,
        },
        Product: {
          screen: ProductScreen,
        },
      },
      {
        initialRouteName: 'Home',
      }
    ),
  },
  Profile: {
    screen: ProfileScreen,
  },
  Cart: {
    screen: CartScreen,
  },
  Orders: {
    screen: OrdersScreen,
  },
  OrderDetail: {
    screen: orderDetailScreen,
  },
  Support: {
    screen: SupportScreen,
  },
},
 {
  contentComponent: SideNav,
  drawerWidth: 250
});
