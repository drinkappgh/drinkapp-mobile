import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { ScrollView, Text, View, TouchableOpacity } from 'react-native';
import { Image, Row, Icon } from '@shoutem/ui';
import { Entypo, FontAwesome } from '@expo/vector-icons';
import navigation from '../services/navigation';


class SideNav extends Component {
  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
    });
    this.props.navigation.dispatch(navigateAction);
  };

  render() {
    return (
      <View>
        <ScrollView style={{ marginTop: 22, alignItems: 'center' }}>
          <View>
            <Image
              styleName="medium-avatar"
              source={{
                uri:
                  'https://s3-us-west-2.amazonaws.com/dave-vortex/grey/wineTime.jpg',
              }}
            />
          </View>
          <View />
        <Row styleName="small" >
            <Entypo
              name="shop"
              size={25}
              color="black"
            />
            <TouchableOpacity  onPress={this.navigateToScreen('Home')}>
            <Text> Store </Text>
            </TouchableOpacity>
          </Row>
          <Row styleName="small">
            <Entypo
              name="shopping-cart"
              size={25}
              color="black"
            />
             <TouchableOpacity  onPress={this.navigateToScreen('Cart')}>
              <Text> Cart </Text>
             </TouchableOpacity>
          </Row>
          <Row styleName="small">
            <Entypo
              name="list"
              size={25}
              color="black"
            />
            <TouchableOpacity  onPress={this.navigateToScreen('Orders')}>
              <Text> Orders </Text>
            </TouchableOpacity>
          </Row>
          <Row styleName="small">
            <FontAwesome
              name="support"
              size={25}
              color="black"
            />
            <TouchableOpacity  onPress={this.navigateToScreen('Support')}>
              <Text> Support </Text>
            </TouchableOpacity>
          </Row>
        </ScrollView>
      </View>
    );
  }
}

SideNav.propTypes = {
  navigation: PropTypes.object,
};

export default SideNav;
